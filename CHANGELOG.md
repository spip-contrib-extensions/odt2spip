# odt2spip — Changelog

## Unreleased

### Added

- #4680 Prise en compte de la légende et de la description des propriétés d'accessibilité d'une image lors de son import
- #4675 Prise en compte des styles "Block Quotation" et "Quote"

### Fixed

- #4682 Ajout d'une espace entre le formatage des items de listes et leur contenu
- #4681 Ajouter un saut de paragraphe après les listes de premier niveau
- #4678 Ne pas ajouter de saut de ligne et de ligne vide après le dernier paragraphe d'une note de bas de page
- #4679 Les "citations" en tout début ou toute fin de document sont correctement prises en charge
- #4677 les retours chariots ne sont plus transformés en `_ `
