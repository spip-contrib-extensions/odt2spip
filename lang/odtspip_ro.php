<?php
/**
 * Fichier de langue : Roumain
 *
 * @author cy_altern
 * @license GNU/LGPL
 *
 * @package plugins
 * @subpackage odt2spip
 * @category import
 *
 * @version $Id$
 *
 */

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'attacher_fichier_odt' => 'Atasezi fisierul ODT la articol?',
 
	// C
	'choix_fichier' => 'Fisierul ODT pentru a fi folosit:',

	// E
	'err_enregistrement_fichier_sortie' => 'Salvare eroare fragment fisier',
	'err_extension_xslt'=> 'Functiile Xslt nu sunt operationale in PHP: cereti administratorului de server activarea extensiilor XSL',
	'err_import_snippet' => 'Eroare la crearea articolului cu plugin Snippets. Asigurativa ca este instalat corespunzator si activat.',
	'err_repertoire_tmp' => 'Eroare: folderul tmp/odt2spip sau subfolderul lui /id_auteur nu a fost creat',
	'err_telechargement_fichier' => 'Eroare: fisierul nu a putut fi recuperat',
	'err_transformation_xslt' => 'Eroare in procesarea fisierului XSLT in ODT',

	// I
	'importer_fichier' => 'Creare de articol din fisierul Ooo Writer',
	   
	// N
	'non' => 'nu',
	   
	// O
	'oui' => 'da',
   
	// T
	'cet_article_version_odt' => 'Versiunea Ooo Writer a articolului' 
);
?>
