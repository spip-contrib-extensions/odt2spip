<?php
/**
 * Fichier de langue : Anglais
 *
 * @author cy_altern
 * @license GNU/LGPL
 *
 * @package plugins
 * @subpackage odt2spip
 * @category import
 *
 * @version $Id$
 *
 */

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'attacher_fichier_odt' => 'Attach the ODT file to the article ?',
	'attacher_fichier_source' => 'Attach the file to the article ?',
	'attacher_fichier_odt_genere' => 'Attach the generated ODT file to the article ?',

	// C
	'cet_article_version_odt' => 'OOo Writer version of this article',
	'choix_fichier' => 'ODT file to use :',
	'choix_fichier_source' => 'Source file',

	// D
	'defaut_attacher' => 'Default choice for ODT file attachment',
	'defaut_attacher_fichier_source' => 'Default choice for source file attachment',
	'defaut_attacher_label' => 'By default, the "Attach file..." field is checked',

	// E
	'err_analyse_odt' => 'Error parsing the ODT file.',
	'err_convertir_fichier' => 'An error occured while converting the file',
	'err_creer_nouvel_objet' => 'Unable to create the new object',
	'err_enregistrement_fichier_sortie' => 'Error saving file snippet',
	'err_extension_fichier' => 'File must have the extension: @extension@',
	'err_extension_xslt'=> 'PHP\'s XSLT functions are not operational in: ask activation XSL extensions to your server administrator',
	'err_import_snippet' => 'Error when creating the article with the plugin Snippets. Make sure it is properly installed and activated.',
	'err_decompresser_fichier' => 'Unable to decompress file',
	'err_recuperer_fichier' => 'There was a problem retrieving the file',
	'err_repertoire_temporaire' => 'Unable to assign a temporary directory',
	'err_repertoire_tmp' => 'Error: tmp/odt2spip folder or its subfolder / id_auteur has not been created',
	'err_telechargement_fichier' => 'Error: the file could not be recovered',
	'err_transformation_xslt' => 'Error processing XSLT file ODT',
	'err_transformation_xslt_mathml' => 'XSLT error when converting MathML',

	// F
	'fichier_traiter_ok' => 'File has been processed',

	// I
	'images_mode_document' => 'Attached images in mode:',
	'importer_fichier' => 'Create an article from an OOo Writer file',
	'importer_fichier_source' => 'Create an article from a file',

	// L
	'langue_publication' => 'Article language',

	// M
	'mode_document' => 'documents',
	'mode_image' => 'images',

	// N
	'non' => 'no',
	'nouvelle_cle_api_generee' => 'A new API key has been created',

	// O
	'oui' => 'yes',

	// R
	'remplacer_article' => 'Remplace article content by the ODT file content',
	'remplacer_article_source' => 'Remplace article content by the file content',

	// T
	'titre_page_configurer' => 'Configuration of odt2spip plugin',

);

?>
